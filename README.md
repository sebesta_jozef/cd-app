# CD APP DEMO

It is a spring boot app for demo purposes.

It contains CD, Performer, User, role endpoints + FE controller

Details are in swagger files/web UI and assignment.

## Installation

### 1. Compile app with:
mvn clean install -DskipTests -Dliquibase.should.run=false

### 2. Start docker DB service
docker-compose up -d db

### 3. Run liquibase DB migrations
mvn liquibase:update

### 4. Start CD App docker container

docker-compose up -d cd-project

## API documentation in swagger

swagger files generated by swagger maven plugin are in target folder:

target/api/openapi.json

target/api/openapi.yaml

### Swagger web UI:

accessible on:

http://localhost:8080/swagger-ui.html

user:admin

pass:admin

## Assignment

task assignment is in czech language in image file assignment_cz.png

(I can make EN translation if necessary)

## FE controller written with thymeleaf

there are two pages:

1. For adding performer and CD
http://localhost:8080/fe/add-performer-and-cd

2. For displaying table of CDS
http://localhost:8080/fe/cd-table

## Authentication

There is basic authentication on endpoints with:

username: admin

password: admin

