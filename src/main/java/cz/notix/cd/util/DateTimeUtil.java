package cz.notix.cd.util;

import lombok.NonNull;
import org.springframework.util.StringUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class DateTimeUtil {
    public static final String ISO_PATTERN = "yyyy-MM-dd'T'HH:mm:ss'Z'";
    public static final String ISO_PATTERN_MILLIS = "yyyy-MM-dd'T'HH:mm:ss.[SSS]'Z'";
    public static final ZoneId ZONE_ID = ZoneId.of("UTC");

    public static ZonedDateTime getNowUtcTime() {
        return ZonedDateTime.now(ZONE_ID);
    }

    public static ZonedDateTime getUtcTimeFromEpochSecond(@NonNull Long epoch) {
        return ZonedDateTime.ofInstant(Instant.ofEpochSecond(epoch), ZONE_ID);
    }

    public static Long getEpochSecondFromUtcTime(@NonNull ZonedDateTime time) {
        return time.toEpochSecond();
    }

    public static ZonedDateTime getUtcTimeFromEpochMilis(@NonNull Long epoch) {
        return ZonedDateTime.ofInstant(Instant.ofEpochMilli(epoch), ZONE_ID);
    }

    public static Long getEpochMilisFromUtcTime(@NonNull ZonedDateTime time) {
        return time.toInstant().toEpochMilli();
    }

    public static String parseJsonDate(@NonNull String inputDate, @NonNull String inputFormat, @NonNull String outputFormat, @NonNull String timeZone) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat(inputFormat);
        String inputDateZone = inputDate;
        if (!StringUtils.isEmpty(timeZone)) {
            ZonedDateTime zonedDateTime = ZonedDateTime.parse(inputDate);
            ZonedDateTime newZonedDateTime = (zonedDateTime != null) ? zonedDateTime.withZoneSameInstant(ZoneId.of(timeZone)) : null;
            inputDateZone = (newZonedDateTime != null) ? DateTimeFormatter.ofPattern(inputFormat).format(newZonedDateTime) : inputDateZone;
        }
        Date outputDate = sdf.parse(inputDateZone);
        sdf.applyPattern(outputFormat);
        return sdf.format(outputDate);
    }
}
