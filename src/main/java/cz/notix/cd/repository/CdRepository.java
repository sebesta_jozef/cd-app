package cz.notix.cd.repository;

import cz.notix.cd.entity.CdEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CdRepository extends JpaRepository<CdEntity, String> {

    Optional<CdEntity> findById(Long id);

    Optional<CdEntity> findByNameAndPerformerId(String name, Long id);

    List<CdEntity> findAllByPerformerId(Long performerId);

    List<CdEntity> getAllByOrderByPerformerNameAsc();

    @Query("SELECT c FROM CdEntity c WHERE year(c.releasedAt) = :year")
    List<CdEntity> findCdsByYearReleased(@Param("year") Integer year);
}
