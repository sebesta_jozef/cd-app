package cz.notix.cd.repository;

import cz.notix.cd.entity.PerformerEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PerformerRepository extends JpaRepository<PerformerEntity, String> {
    Optional<PerformerEntity> findByName(String name);

    List<PerformerEntity> findAllByOrderByNameAsc();

    Optional<PerformerEntity> findById(Long id);
}
