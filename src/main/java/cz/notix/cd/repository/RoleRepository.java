package cz.notix.cd.repository;

import cz.notix.cd.entity.RoleEntity;
import cz.notix.cd.enums.RoleEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<RoleEntity, Long> {

    Optional<RoleEntity> findRoleEntityByName(RoleEnum name);
}
