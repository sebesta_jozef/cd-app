package cz.notix.cd.controller;

import cz.notix.cd.dto.CdDto;
import cz.notix.cd.dto.ResponseError;

import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.security.Principal;
import java.util.List;

@Api(tags="Cd", description= "Controller to create Cd, update and retrieve Cds")
public interface CdController {

    @Operation(summary = "Get CD by ID", responses = {
            @ApiResponse(responseCode = "200", description = "CD returned",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = CdDto.class))),
            @ApiResponse(responseCode = "400", description = "Malformed id of CD"),
            @ApiResponse(responseCode = "404", description = "CD by given CD ID not found",
                    content = @Content(
                            schema = @Schema(implementation = ResponseError.class),
                            mediaType = MediaType.APPLICATION_JSON_VALUE
                    )),
            @ApiResponse(responseCode = "401", description = "Unauthorized"),
            @ApiResponse(responseCode = "403", description = "User doest have sufficient privileges for his role to access protected URL"),
            @ApiResponse(responseCode = "500", description = "Unexpected application error")})
    @GET
    @Path("/{id}")
    CdDto getById(@PathParam("id") @Parameter(description = "ID of CD") Long id);



    @Operation(summary = "Get all CDs", responses = {
            @ApiResponse(responseCode = "200", description = "All CDs returned",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            array = @ArraySchema(schema = @Schema(implementation = CdDto.class)))),
            @ApiResponse(responseCode = "401", description = "Unauthorized"),
            @ApiResponse(responseCode = "403", description = "User doest have sufficient privileges for his role to access protected URL"),
            @ApiResponse(responseCode = "500", description = "Unexpected application error")})
    @GET
    @Path("/")
    List<CdDto> getCDs();

    @Operation(summary = "Get CDs by year of CD release", responses = {
            @ApiResponse(responseCode = "200", description = "CDs by given year release returned",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            array = @ArraySchema(schema = @Schema(implementation = CdDto.class)))),
            @ApiResponse(responseCode = "400", description = "Malformed year"),
            @ApiResponse(responseCode = "401", description = "Unauthorized"),
            @ApiResponse(responseCode = "403", description = "User doest have sufficient privileges for his role to access protected URL"),
            @ApiResponse(responseCode = "500", description = "Unexpected application error")})
    @GET
    @Path("/year-released/{year}")
    List<CdDto> getCdsByYearReleased(@PathParam("year") @Parameter(description = "Year of CD release") Integer id);

    @Operation(summary = "Create CD", responses = {
            @ApiResponse(responseCode = "200", description = "CD created"),
            @ApiResponse(responseCode = "400", description = "Wrong submitted data in request body"),
            @ApiResponse(responseCode = "401", description = "Unauthorized"),
            @ApiResponse(responseCode = "403", description = "User doest have sufficient privileges for his role to access protected URL"),
            @ApiResponse(responseCode = "500", description = "Unexpected application error")})
    @POST
    @Path("/")
    void createCd(@RequestBody(required = true, description = "CD data") CdDto cdDTO, Principal principal);

    @Operation(summary = "Update CD", responses = {
            @ApiResponse(responseCode = "200", description = "CD updated"),
            @ApiResponse(responseCode = "400", description = "Wrong submitted data in request body or malformed ID of CD"),
            @ApiResponse(responseCode = "404", description = "CDs by ID not found",
                    content = @Content(
                            schema = @Schema(implementation = ResponseError.class),
                            mediaType = MediaType.APPLICATION_JSON_VALUE
                    )),
            @ApiResponse(responseCode = "401", description = "Unauthorized"),
            @ApiResponse(responseCode = "403", description = "User doest have sufficient privileges for his role to access protected URL"),
            @ApiResponse(responseCode = "500", description = "Unexpected application error")})
    @PUT
    @Path("/{id}")
    void updateCd(@PathParam("id") @Parameter(description = "ID of CD") Long id,
                  @RequestBody(required = true, description = "CD data to be updated") CdDto cdDTO, Principal principal);


    @Operation(summary = "Upload image to given CD", responses = {
            @ApiResponse(responseCode = "200", description = "Image uploaded to given CD"),
            @ApiResponse(responseCode = "400", description = "Unsupported file extension or malformed ID of CD"),
            @ApiResponse(responseCode = "404", description = "CD by ID not found",
                    content = @Content(
                            schema = @Schema(implementation = ResponseError.class),
                            mediaType = MediaType.APPLICATION_JSON_VALUE
                    )),
            @ApiResponse(responseCode = "401", description = "Unauthorized"),
            @ApiResponse(responseCode = "403", description = "User doest have sufficient privileges for his role to access protected URL"),
            @ApiResponse(responseCode = "500", description = "Unexpected application error")})
    @PUT
    @Path("/{id}/upload-image")
    void uploadImage(@PathParam("id") @Parameter(description = "ID of CD") Long id,
                     @RequestParam MultipartFile file, Principal principal);
}