package cz.notix.cd.controller;

import cz.notix.cd.dto.UserDto;
import cz.notix.cd.enums.RoleEnum;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.MediaType;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.util.List;

@Api(tags="Role", description= "Controller to get users by role")
public interface RoleController {

    @Operation(summary = "Get all users by given role name", responses = {
            @ApiResponse(responseCode = "200", description = "All users by given role name returned",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            array = @ArraySchema(schema = @Schema(implementation = UserDto.class)))),
            @ApiResponse(responseCode = "401", description = "Unauthorized"),
            @ApiResponse(responseCode = "403", description = "User doest have sufficient privileges for his role to access protected URL"),
            @ApiResponse(responseCode = "500", description = "Unexpected application error")})
    @GET
    @Path("/{roleName}/user")
    List<UserDto> getUsersByRoleName(@PathParam("roleName") @Parameter(description = "Role name") RoleEnum roleName);
}
