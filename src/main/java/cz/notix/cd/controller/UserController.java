package cz.notix.cd.controller;

import cz.notix.cd.dto.UserDto;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

import javax.ws.rs.POST;
import javax.ws.rs.Path;

@Api(tags="User", description= "Controller to create user")
public interface UserController {

    @Operation(summary = "Create user", responses = {
            @ApiResponse(responseCode = "200", description = "User created"),
            @ApiResponse(responseCode = "400", description = "Wrong submitted data in request body"),
            @ApiResponse(responseCode = "401", description = "Unauthorized"),
            @ApiResponse(responseCode = "403", description = "User doest have sufficient privileges for his role to access protected URL"),
            @ApiResponse(responseCode = "500", description = "Unexpected application error")})
    @POST
    @Path("/")
    void createUser(@RequestBody(required = true, description = "User data") UserDto userDto);
}
