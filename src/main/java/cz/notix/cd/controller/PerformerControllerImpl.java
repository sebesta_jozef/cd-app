package cz.notix.cd.controller;

import cz.notix.cd.dto.CdDto;
import cz.notix.cd.service.CdService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("performer")
public class PerformerControllerImpl implements PerformerController {

    private final CdService cdService;

    @Override
    @PreAuthorize("hasAuthority('READ_PRIVILEGE')")
    @GetMapping({"/{id}/cd"})
    public List<CdDto> getCdByPerformerId(@PathVariable("id") final Long id) {
        return cdService.getCdsByPerformerId((id));
    }
}
