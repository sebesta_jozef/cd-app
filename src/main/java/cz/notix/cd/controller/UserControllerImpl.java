package cz.notix.cd.controller;

import cz.notix.cd.dto.UserDto;
import cz.notix.cd.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("user")
public class UserControllerImpl implements UserController {

    private final UserService userService;

    @Override
    @PreAuthorize("hasAuthority('WRITE_PRIVILEGE')")
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void createUser(@Valid @RequestBody final UserDto userDto) {
        userService.createUser(userDto);
    }
}
