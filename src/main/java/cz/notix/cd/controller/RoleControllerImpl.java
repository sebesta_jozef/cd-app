package cz.notix.cd.controller;

import cz.notix.cd.dto.UserDto;
import cz.notix.cd.enums.RoleEnum;
import cz.notix.cd.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("role")
public class RoleControllerImpl implements RoleController {

    private final UserService userService;

    @Override
    @PreAuthorize("hasAuthority('READ_PRIVILEGE')")
    @GetMapping("/{roleName}/user")
        public List<UserDto> getUsersByRoleName(@PathVariable("roleName") RoleEnum roleName) {
        return userService.getUsersByRoleName(roleName);
    }
}
