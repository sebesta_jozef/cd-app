package cz.notix.cd.controller;

import cz.notix.cd.dto.CdDto;
import cz.notix.cd.dto.ResponseError;
import io.swagger.annotations.Api;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.MediaType;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.util.List;

@Api(tags="Performer", description= "Controller to get cds by performer")
public interface PerformerController {

    @Operation(summary = "Get CD by performer ID", responses = {
            @ApiResponse(responseCode = "200", description = "CDs by given performer returned",
                    content = @Content(
                            mediaType = MediaType.APPLICATION_JSON_VALUE,
                            array = @ArraySchema(schema = @Schema(implementation = CdDto.class)))),
            @ApiResponse(responseCode = "400", description = "Malformed performer id"),
            @ApiResponse(responseCode = "404", description = "CDs by given performer ID not found",
                    content = @Content(
                            schema = @Schema(implementation = ResponseError.class),
                            mediaType = MediaType.APPLICATION_JSON_VALUE
                    )),
            @ApiResponse(responseCode = "401", description = "Unauthorized"),
            @ApiResponse(responseCode = "403", description = "User doest have sufficient privileges for his role to access protected URL"),
            @ApiResponse(responseCode = "500", description = "Unexpected application error")})
    @GET
    @Path("/{id}/cd")
    List<CdDto> getCdByPerformerId(@PathParam("id") @Parameter(description = "ID of performer") Long id);
}
