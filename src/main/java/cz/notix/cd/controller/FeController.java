package cz.notix.cd.controller;

import cz.notix.cd.dto.CdDto;
import cz.notix.cd.dto.PerformerDto;
import cz.notix.cd.exception.BadRequestException;
import cz.notix.cd.service.CdService;
import cz.notix.cd.service.PerformerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@Slf4j
@Controller
@RequestMapping("fe")
@RequiredArgsConstructor
public class FeController {

    private final PerformerService performerService;
    private final CdService cdService;

    @PreAuthorize("hasAuthority('READ_PRIVILEGE')")
    @GetMapping("/add-performer-and-cd")
    public String showIndexPage(@ModelAttribute("performer") PerformerDto performerDto, @ModelAttribute("cd") CdDto cdDto, Model model) {
        model.addAttribute("performer", performerDto);
        model.addAttribute("cd", cdDto);
        List<PerformerDto> performers = performerService.getAllPerformers();
        model.addAttribute("performers", performers);
        return "add_performer_and_cd_page";
    }

    @PreAuthorize("hasAuthority('READ_PRIVILEGE')")
    @GetMapping("/cd-table")
    public String showCdTable(Model model) {
        List<CdDto> cds = cdService.getCdsGroupByPerformerName();
        model.addAttribute("cds", cds);
        return "cd_table_page";
    }

    @PreAuthorize("hasAuthority('READ_PRIVILEGE')")
    @PostMapping(value = "/performer/create")
    public String createPerformer(@Valid @ModelAttribute("performer") PerformerDto performerDto, BindingResult result, RedirectAttributes redirAttrs, Model model) {
        if (!result.hasErrors()) {
            model.addAttribute("performer", performerDto);
            try {
                performerService.createPerformer(performerDto);
                redirAttrs.addFlashAttribute("success", "Performer saved");
                return "redirect:/fe/add-performer-and-cd";
            } catch (BadRequestException e) {
                if (e.getCode().equals(BadRequestException.Code.PERFORMER_ALREADY_EXISTS)) {
                    result.rejectValue("name", "error.name", e.getMessage());
                }
            } catch (Exception e) {
                log.error("unexpected error", e);
                redirAttrs.addFlashAttribute("error", "Unexpected error " + e.getMessage());
                return "redirect:/fe/add-performer-and-cd";
            }
        }
        model.addAttribute("cd", new CdDto());
        return "add_performer_and_cd_page";
    }

    @PreAuthorize("hasAuthority('READ_PRIVILEGE')")
    @PostMapping(value = "/cd/create")
    public String createCd(@Valid @ModelAttribute("cd") CdDto cdDto, BindingResult result, Model model, RedirectAttributes redirAttrs, Principal principal) {
        if (StringUtils.isEmpty(cdDto.getPerformerId())) {
            result.rejectValue("performerId", "error.performerId", "must not be empty");
        }
        if (!result.hasErrors()) {
            try {
                cdService.createCd(cdDto, principal);
                redirAttrs.addFlashAttribute("success", "CD saved");
                return "redirect:/fe/add-performer-and-cd";
            } catch (BadRequestException e) {
                if (e.getCode().equals(BadRequestException.Code.CD_ALREADY_EXISTS)) {
                    result.rejectValue("name", "error.name", e.getMessage());
                }
            } catch (Exception e) {
                log.error("unexpected error", e);
                redirAttrs.addFlashAttribute("error", "Unexpected error " + e.getMessage());
                return "redirect:/fe/add-performer-and-cd";
            }
            model.addAttribute("cd", cdDto);
        }
        model.addAttribute("performer", new PerformerDto());
        List<PerformerDto> performers = performerService.getAllPerformers();
        model.addAttribute("performers", performers);
        return "add_performer_and_cd_page";
    }
}
