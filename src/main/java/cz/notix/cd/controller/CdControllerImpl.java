package cz.notix.cd.controller;

import cz.notix.cd.dto.CdDto;
import cz.notix.cd.service.CdService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RequestMapping("cd")
@RequiredArgsConstructor
@RestController
public class CdControllerImpl implements CdController {

    private final CdService cDService;

    @Override
    @PreAuthorize("hasAuthority('READ_PRIVILEGE')")
    @GetMapping
    public List<CdDto> getCDs() {
        return cDService.getAllCDs();
    }

    @Override
    @PreAuthorize("hasAuthority('READ_PRIVILEGE')")
    @GetMapping("/{id}")
    public CdDto getById(@PathVariable("id") final Long id) {
        return cDService.getCdById(id);
    }

    @Override
    @PreAuthorize("hasAuthority('WRITE_PRIVILEGE')")
    @PostMapping
    public void createCd(@Valid @RequestBody final CdDto cdDTO, Principal principal) {
        cDService.createCd(cdDTO, principal);
    }

    @Override
    @PreAuthorize("hasAuthority('WRITE_PRIVILEGE')")
    @PutMapping("/{id}")
    public void updateCd(@PathVariable("id") final Long id, @Valid @RequestBody final CdDto cdDto, Principal principal) {
        cDService.updateCd(id, cdDto, principal);
    }

    @Override
    @PreAuthorize("hasAuthority('READ_PRIVILEGE')")
    @GetMapping({"/year-released/{year}"})
    public List<CdDto> getCdsByYearReleased(@PathVariable("year") final Integer year) {
        return cDService.getCdsByYearReleased(year);
    }

    @Override
    @PreAuthorize("hasAuthority('WRITE_PRIVILEGE')")
    @PutMapping({ "/{id}/upload-image"})
    public void uploadImage(@PathVariable("id") final Long id, @RequestParam("file") MultipartFile file, Principal principal) {
        cDService.uploadImageToCd(id, file, principal);
    }
}
