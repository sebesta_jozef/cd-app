package cz.notix.cd.exception;

import lombok.Data;

@Data
public class ObjectNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -6245655314604522203L;

    public enum Code {
        CD_NOT_FOUND, PERFORMER_NOT_FOUND
    }

    private Code code;

    public ObjectNotFoundException(Code code, String message) {
        super(message);
        this.code = code;
    }
}
