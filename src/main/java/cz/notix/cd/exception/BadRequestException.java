package cz.notix.cd.exception;

import lombok.Data;

@Data
public class BadRequestException extends RuntimeException {

    private static final long serialVersionUID = -6245655314604522203L;

    public enum Code {
        CORRUPTED_IMAGE, UNSUPPORTED_FILE_EXTENSION, USER_ALREADY_EXISTS, ROLE_NOT_FOUND, INVALID_DATA,
        CD_ALREADY_EXISTS, PERFORMER_ALREADY_EXISTS
    }

    private Code code;

    public BadRequestException(Code code, String message) {
        super(message);
        this.code = code;
    }
}
