package cz.notix.cd.handler;

import cz.notix.cd.dto.Response;
import cz.notix.cd.dto.ResponseError;
import cz.notix.cd.dto.ResponseErrorDetail;
import cz.notix.cd.exception.BadRequestException;
import cz.notix.cd.exception.ObjectNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@ControllerAdvice
public class ControllerErrorHandler {

    @Order(Ordered.HIGHEST_PRECEDENCE)
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Response handleValidationExceptions(MethodArgumentNotValidException e) {
        List<ResponseErrorDetail> responseErrorDetailList = new ArrayList<>();
        e.getBindingResult().getAllErrors().forEach((error) -> {
            responseErrorDetailList.add(new ResponseErrorDetail(((FieldError) error).getField(), error.getDefaultMessage()));

        });
        log.error("Validation exception", e);
        return new Response(HttpStatus.BAD_REQUEST.value(),
                new ResponseError(BadRequestException.Code.INVALID_DATA.name(), responseErrorDetailList));
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @Order(Ordered.LOWEST_PRECEDENCE)
    @ResponseBody
    public Response handleException(final BadRequestException e) {
        log.error("BadRequest exception", e);
        return new Response(HttpStatus.BAD_REQUEST.value(),
                new ResponseError(e.getCode().name(), e.getMessage()));
    }

    @Order(Ordered.HIGHEST_PRECEDENCE)
    @ExceptionHandler(ObjectNotFoundException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @ResponseBody
    public Response handleException(final ObjectNotFoundException e) {
        log.error("ObjectNotFound exception", e);
        return new Response(HttpStatus.NOT_FOUND.value(),
                new ResponseError(e.getCode().name(), e.getMessage()));
    }

    @ExceptionHandler(Exception.class)
    @Order(Ordered.LOWEST_PRECEDENCE)
    @ResponseBody
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public Response fallback(Exception e) {
        log.error("Unexpected exception", e);
        return new Response(HttpStatus.INTERNAL_SERVER_ERROR.value(),
                new ResponseError("INTERNAL_SERVER_ERROR", e.getMessage()));
    }
}
