package cz.notix.cd.converter;

import cz.notix.cd.enums.RoleEnum;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class RoleEnumConverter implements Converter<String, RoleEnum> {
    @Override
    public RoleEnum convert(String value) {
        return RoleEnum.valueOf(value.toUpperCase());
    }
}
