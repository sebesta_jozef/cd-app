package cz.notix.cd.enums;

public enum PrivilegeEnum {
    READ_PRIVILEGE,
    WRITE_PRIVILEGE,
}
