package cz.notix.cd.service;

import cz.notix.cd.configuration.AppConfig;
import cz.notix.cd.dto.CdDto;
import cz.notix.cd.dto.mapper.CdMapper;
import cz.notix.cd.entity.CdEntity;
import cz.notix.cd.entity.PerformerEntity;
import cz.notix.cd.exception.BadRequestException;
import cz.notix.cd.exception.ObjectNotFoundException;
import cz.notix.cd.repository.CdRepository;
import cz.notix.cd.repository.PerformerRepository;
import liquibase.util.file.FilenameUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;


import java.io.IOException;
import java.security.Principal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class CdService {

    private final AppConfig appConfig;
    private final CdRepository cdRepository;
    private final PerformerRepository performerRepository;
    private final CdMapper cdMapper;

    public List<CdDto> getAllCDs() {
        List<CdDto> cDs = cdRepository.findAll().stream().map(cd -> cdMapper.cdEntityToCdDto(cd)).collect(Collectors.toList());
        log.info("CDs successfully retrieved");
        return cDs;
    }

    public CdDto getCdById(Long id) {
        CdEntity cdEntity = cdRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException(ObjectNotFoundException.Code.CD_NOT_FOUND, "CD Not found"));
        log.info("CD successfully retrieved with ID {}", id);
        return  cdMapper.cdEntityToCdDto(cdEntity);
    }

    public PerformerEntity getPerformerEntity(CdDto cdDto) {
        Optional<PerformerEntity> performerEntity;
        if (cdDto.getPerformerId() != null) {
            performerEntity = performerRepository.findById(cdDto.getPerformerId());

        } else if (!StringUtils.isEmpty(cdDto.getPerformer().getName())) {
            performerEntity = performerRepository.findByName(cdDto.getPerformer().getName());
        } else {
            throw new IllegalStateException("Missing performer in DTO");
        }
        if (performerEntity.isPresent()) {
            return performerEntity.get();

        } else {
            throw new ObjectNotFoundException(ObjectNotFoundException.Code.PERFORMER_NOT_FOUND, "Performer not found");
        }
    }

    public void createCd(CdDto cdDto, Principal principal) {
        PerformerEntity performerEntity = getPerformerEntity(cdDto);
        cdRepository.findByNameAndPerformerId(cdDto.getName(), performerEntity.getId())
                .ifPresent(c -> { throw new BadRequestException(BadRequestException.Code.CD_ALREADY_EXISTS, "CD with name " + c.getName() + " for performer " + performerEntity.getName() + " already exists");});
        CdEntity cdEntity = cdMapper.cdDtoToCdEntity(cdDto);
        cdEntity.setPerformer(performerEntity);
        cdEntity.setCreatedAt(new Date());
        cdEntity.setCreatedBy(principal.getName());
        cdRepository.save(cdEntity);
        log.info("CD successfully created with ID {}", cdEntity.getId());
    }

    public void updateCd(Long id, CdDto cdDTO, Principal principal) {
        CdEntity cdEntity = cdRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException(ObjectNotFoundException.Code.CD_NOT_FOUND, "CD Not found"));

        CdEntity updatedCdEntity = cdMapper.updateCdEntityFromCdDto(cdEntity, cdDTO);

        if (!Objects.isNull(cdDTO.getPerformer())) {
            PerformerEntity performerEntity = performerRepository.findByName(cdDTO.getPerformer().getName())
                    .orElseThrow(() -> new ObjectNotFoundException(ObjectNotFoundException.Code.PERFORMER_NOT_FOUND, "Performer " + cdDTO.getPerformer().getName() + " not found"));
            updatedCdEntity.setPerformer(performerEntity);
        }
        updatedCdEntity.setModifiedAt(new Date());
        updatedCdEntity.setModifiedBy(principal.getName());
        cdRepository.save(updatedCdEntity);
        log.info("CD successfully updated with ID {}", updatedCdEntity.getId());
    }

    public List<CdDto> getCdsByPerformerId(Long performerId) {
        List<CdDto> cDs = cdRepository.findAllByPerformerId(performerId).stream().map(cd -> cdMapper.cdEntityToCdDto(cd)).collect(Collectors.toList());
        log.info("CDs successfully retrieved with performer ID {}", performerId);
        return cDs;
    }

    public List<CdDto> getCdsByYearReleased(Integer year) {
        List<CdDto> cDs = cdRepository.findCdsByYearReleased(year).stream().map(cd -> cdMapper.cdEntityToCdDto(cd)).collect(Collectors.toList());
        log.info("CDs successfully retrieved with year {}", year);
        return cDs;
    }

    public List<CdDto> getCdsGroupByPerformerName() {
        List<CdDto> cDs = cdRepository.getAllByOrderByPerformerNameAsc().stream().map(cd -> cdMapper.cdEntityToCdDto(cd)).collect(Collectors.toList());
        log.info("CDs grouped by performer name successfully retrieved ");
        return cDs;
    }

    public void uploadImageToCd(Long id, MultipartFile file, Principal principal) {
        CdEntity cdEntity = cdRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException(ObjectNotFoundException.Code.CD_NOT_FOUND, "CD Not found"));

        String fileExtension = FilenameUtils.getExtension(file.getOriginalFilename());
        if (!Arrays.asList(appConfig.getSupportedImageFormats()).contains(fileExtension)) {
            throw new BadRequestException(BadRequestException.Code.UNSUPPORTED_FILE_EXTENSION, "File extension " + fileExtension + " is not supported");
        }

        try {
            cdEntity.setImage(file.getBytes());
        } catch (IOException e) {
            throw new BadRequestException(BadRequestException.Code.CORRUPTED_IMAGE,"Unable to get content of image file");
        }
        cdEntity.setModifiedAt(new Date());
        cdEntity.setModifiedBy(principal.getName());
        cdRepository.save(cdEntity);
        log.info("Image successfully uploaded to CD with ID {}", cdEntity.getId());
    }
}
