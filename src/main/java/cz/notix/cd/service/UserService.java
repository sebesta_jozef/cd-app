package cz.notix.cd.service;

import cz.notix.cd.dto.mapper.UserMapper;
import cz.notix.cd.exception.BadRequestException;
import lombok.RequiredArgsConstructor;
import org.springframework.util.ObjectUtils;
import cz.notix.cd.dto.UserDto;
import cz.notix.cd.entity.RoleEntity;
import cz.notix.cd.entity.UserEntity;
import cz.notix.cd.enums.RoleEnum;
import cz.notix.cd.repository.RoleRepository;
import cz.notix.cd.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Slf4j
@Service
public class UserService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final RoleRepository roleRepository;

    public void createUser(UserDto userDto) {
        userRepository.findByUsername(userDto.getUsername())
                .ifPresent(u -> { throw new BadRequestException(BadRequestException.Code.USER_ALREADY_EXISTS, "User with username " + u.getUsername() + " already exists");});

        UserEntity userEntity = userMapper.userDtoToUserEntity(userDto);
        RoleEnum userRoleEnum = !ObjectUtils.isEmpty(userDto.getRole()) ? userDto.getRole() :RoleEnum.ROLE_USER;

        RoleEntity roleEntity = roleRepository.findRoleEntityByName(userRoleEnum)
        .orElseThrow(() -> new BadRequestException(BadRequestException.Code.ROLE_NOT_FOUND, "Role not found"));

        userEntity.setRole(roleEntity);
        userEntity.setPassword(passwordEncoder.encode("secret"));
        userRepository.save(userEntity);
        log.info("User successfully created with ID {}", userEntity.getId());
    }

    public List<UserDto> getUsersByRoleName(RoleEnum roleName) {
        List<UserDto> users = userRepository.findAllByRoleName(roleName).stream().map(user -> userMapper.userEntityToUserDto(user)).collect(Collectors.toList());
        log.info("Users retrieved with role name {}", roleName);
        return users;
    }
}
