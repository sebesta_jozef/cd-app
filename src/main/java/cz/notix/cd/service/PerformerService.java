package cz.notix.cd.service;

import cz.notix.cd.dto.CdDto;
import cz.notix.cd.dto.PerformerDto;
import cz.notix.cd.dto.mapper.PerformerMapper;
import cz.notix.cd.entity.PerformerEntity;
import cz.notix.cd.exception.BadRequestException;
import cz.notix.cd.repository.PerformerRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class PerformerService {

    private final PerformerRepository performerRepository;
    private final PerformerMapper performerMapper;

    public void createPerformer(PerformerDto performerDto) {
        performerRepository.findByName(performerDto.getName())
                .ifPresent(p -> { throw new BadRequestException(BadRequestException.Code.PERFORMER_ALREADY_EXISTS, "Performer with name " + p.getName() + " already exists");});

        performerRepository.save(performerMapper.performerDtoToPerformerEntity(performerDto));
    }

    public List<PerformerDto> getAllPerformers() {
        List<PerformerDto> performers = performerRepository.findAllByOrderByNameAsc().stream().map(p -> performerMapper.performerEntityToPerformerDto(p)).collect(Collectors.toList());
        log.info("Performers successfully retrieved");
        return performers;
    }
}
