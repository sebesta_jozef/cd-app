package cz.notix.cd.service;

import cz.notix.cd.authentication.CustomUserDetails;
import cz.notix.cd.entity.PrivilegeEntity;
import cz.notix.cd.entity.RoleEntity;
import cz.notix.cd.entity.UserEntity;
import cz.notix.cd.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<UserEntity> userEntity = userRepository.findByUsername(username);
        if (!userEntity.isPresent()) {
            throw new UsernameNotFoundException("User not found");
        } else {
            return new CustomUserDetails(userEntity.get(), getAuthorities(userEntity.get().getRole()));
        }
    }

    private Collection<? extends GrantedAuthority> getAuthorities(
            RoleEntity role) {

        return getGrantedAuthorities(getPrivileges(role));
    }

    private List<String> getPrivileges(RoleEntity role) {
        List<String> privileges = new ArrayList<>();
        List<PrivilegeEntity> collection = new ArrayList<>(role.getPrivileges());
        for (PrivilegeEntity item : collection) {
            privileges.add(item.getName().name());
        }
        return privileges;
    }

    private List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String privilege : privileges) {
            authorities.add(new SimpleGrantedAuthority(privilege));
        }
        return authorities;
    }
}