package cz.notix.cd.entity;

import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;
import java.util.Date;

@Data
@Entity
@Table(name = "CD", uniqueConstraints = {@UniqueConstraint(columnNames = {"NAME", "PERFORMER_ID"})})
public class CdEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequence-generator")
    @SequenceGenerator(name = "sequence-generator", sequenceName = "cd_id_seq" ,allocationSize = 1)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED_AT", nullable = false)
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "MODIFIED_AT", nullable = true)
    private Date modifiedAt;

    @Column(name = "CREATED_BY", nullable = false)
    private String createdBy;

    @Column(name = "MODIFIED_BY", nullable = true)
    private String modifiedBy;

    @Column(name = "NAME", nullable = false)
    private String name;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH})
    @JoinColumn(name = "PERFORMER_ID", nullable = false)
    private PerformerEntity performer;

    @Column(name = "RELEASED_AT", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date releasedAt;

    @Lob
    @Type(type = "org.hibernate.type.BinaryType")
    @Column(name = "IMAGE", nullable = true, columnDefinition = "BLOB")
    private byte[] image;
}
