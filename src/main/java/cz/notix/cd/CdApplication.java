package cz.notix.cd;

import cz.notix.cd.configuration.AppConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@EnableConfigurationProperties(AppConfig.class)
@SpringBootApplication
public class CdApplication {

	public static void main(String[] args) {
		SpringApplication.run(CdApplication.class, args);
	}

}
