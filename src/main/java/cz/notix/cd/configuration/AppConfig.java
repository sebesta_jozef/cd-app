package cz.notix.cd.configuration;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "app")
public class AppConfig {

    @Value("#{'${cd.image.listOfSupportedFormats}'.toLowerCase().split(',')}")
    private String[] supportedImageFormats;
}
