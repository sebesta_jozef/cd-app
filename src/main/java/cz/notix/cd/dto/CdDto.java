package cz.notix.cd.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@Schema(description = "CD object")
public class CdDto {

    @NotBlank
    @JsonProperty(value = "nazev")
    @ApiModelProperty(required = true, value = "Name of CD")
    private String name;

    @JsonProperty(value = "interpret_id", access = JsonProperty.Access.WRITE_ONLY)
    @ApiModelProperty(required = false, value = "Performer ID")
    private Long performerId;

    @JsonProperty(value = "interpret")
    @ApiModelProperty(required = true, value = "Performer object")
    private PerformerDto performer;

    @Pattern(regexp = "^(0[1-9]|[12][0-9]|3[01])\\.(0[1-9]|1[012])\\.((19|2[0-9])[0-9]{2})$", message = "This is not valid date. Date has to be in format dd.MM.yyyy")
    @NotBlank
    @JsonProperty(value = "datum_vydani")
    @ApiModelProperty(required = true, value = "Release date of CD")
    private String releasedAt;

    @JsonProperty(value = "vytvoril")
    @ApiModelProperty(required = true, value = "Created by")
    private String createdBy;

    @JsonProperty(value = "modifikoval")
    @ApiModelProperty(required = false, value = "Modified by")
    private String modifiedBy;

    @JsonProperty(value = "vytvoreno")
    @ApiModelProperty(required = true, value = "Created at")
    private String createdAt;

    @JsonProperty(value = "modifikovano")
    @ApiModelProperty(required = false, value = "Modified at")
    private String modifiedAt;
}
