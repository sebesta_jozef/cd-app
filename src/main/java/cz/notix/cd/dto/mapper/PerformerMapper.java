package cz.notix.cd.dto.mapper;

import cz.notix.cd.dto.PerformerDto;
import cz.notix.cd.entity.PerformerEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring")
public interface PerformerMapper {

    @Mappings({
            @Mapping(target="name", source="name"),
            @Mapping(target="foundedAt", source="foundationYear", dateFormat = "yyyy"),
            @Mapping(target="description", source="description"),
    })
    PerformerEntity performerDtoToPerformerEntity(PerformerDto performerDto);

    @Mappings({
            @Mapping(target="name", source="name"),
            @Mapping(target="id", source="id"),
            @Mapping(target="foundationYear", source="foundedAt", dateFormat = "yyyy"),
            @Mapping(target="description", source="description"),
    })
    PerformerDto performerEntityToPerformerDto(PerformerEntity performerEntity);
}
