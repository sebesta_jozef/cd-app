package cz.notix.cd.dto.mapper;

import cz.notix.cd.dto.CdDto;
import cz.notix.cd.entity.CdEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import org.mapstruct.NullValuePropertyMappingStrategy;


@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface CdMapper {
    @Mappings({
            @Mapping(target="name", source="name"),
            @Mapping(target="performerId", source = "performer.id"),
            @Mapping(target="performer.name", source="performer.name"),
            @Mapping(target="performer.foundationYear", source="performer.foundedAt", dateFormat = "yyyy"),
            @Mapping(target="performer.description", source="performer.description"),
            @Mapping(target="releasedAt", source="releasedAt", dateFormat = "dd.MM.yyyy"),
            @Mapping(target="createdBy", source="createdBy"),
            @Mapping(target="modifiedBy", source="modifiedBy", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_DEFAULT),
            @Mapping(target="modifiedAt", source="modifiedAt", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_DEFAULT, dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'"),
            @Mapping(target="createdAt", source="createdAt", dateFormat = "yyyy-MM-dd'T'HH:mm:ss'Z'")
    })
    CdDto cdEntityToCdDto(CdEntity cdEntity);
    @Mappings({
            @Mapping(target="name", source="name"),
            @Mapping(target="performer.foundedAt", source="performer.foundationYear", dateFormat = "yyyy"),
            @Mapping(target="performer.description", source="performer.description"),
            @Mapping(target="releasedAt", source="releasedAt", dateFormat = "dd.MM.yyyy")
    })
    CdEntity cdDtoToCdEntity(CdDto cdDTO);

    @Mappings({
            @Mapping(target="name", source="name"),
            @Mapping(target="performer.foundedAt", source="performer.foundationYear", dateFormat = "yyyy"),
            @Mapping(target="performer.description", source="performer.description"),
            @Mapping(target="releasedAt", source="releasedAt", dateFormat = "dd.MM.yyyy")
    })
    CdEntity updateCdEntityFromCdDto(@MappingTarget CdEntity cdEntity, CdDto cdDTO);
}
