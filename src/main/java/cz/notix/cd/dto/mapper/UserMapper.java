package cz.notix.cd.dto.mapper;

import cz.notix.cd.dto.UserDto;
import cz.notix.cd.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;


@Mapper(componentModel = "spring")
public interface UserMapper {
    @Mappings({
            @Mapping(target = "username", source = "username"),
            @Mapping(target = "role", source = "role"),
    })
    UserEntity userDtoToUserEntity(UserDto userDto);

    @Mappings({
            @Mapping(target = "username", source = "username"),
            @Mapping(target = "role", source = "role.name"),
    })
    UserDto userEntityToUserDto(UserEntity userEntity);
}