package cz.notix.cd.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Schema(description = "Performer object")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class PerformerDto {

    @ApiModelProperty(required = true, value = "ID of performer")
    private Long id;

    @NotBlank
    @JsonProperty(value = "nazev")
    @ApiModelProperty(required = true, value = "Name of performer")
    private String name;

    @NotBlank
    @JsonProperty(value = "popis")
    @ApiModelProperty(required = true, value = "Description of performer")
    private String description;

    @Pattern(regexp="^(19|20)\\d{2}$", message = "This is not valid year")
    @NotBlank
    @ApiModelProperty(required = true, value = "Foundation year of performer")
    @JsonProperty(value = "rok_zalozeni")
    private String foundationYear;
}
