package cz.notix.cd.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import cz.notix.cd.enums.RoleEnum;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
@Schema(description = "User object")
public class UserDto implements Serializable {

    @NotBlank
    @JsonProperty(value = "uzivatelske_jmeno")
    @ApiModelProperty(required = true, value = "Username")
    private String username;

    @JsonProperty(value = "role", required = true)
    @ApiModelProperty(required = false, value = "Role")
    private RoleEnum role;
}
