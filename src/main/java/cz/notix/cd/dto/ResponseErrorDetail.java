package cz.notix.cd.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Schema(description = "Error response detail")
@Data
public class ResponseErrorDetail {

    @ApiModelProperty(required = true, value = "Field name")
    @JsonProperty(value = "nazev_pole")
    private String fieldName;

    @ApiModelProperty(required = true, value = "Error message")
    @JsonProperty(value = "chybova_zprava")
    private String errorMessage;

    public ResponseErrorDetail(String fieldName, String errorMessage) {
        this.fieldName = fieldName;
        this.errorMessage = errorMessage;
    }
}
